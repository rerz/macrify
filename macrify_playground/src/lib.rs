use macrify::ast_traits::impl_block::IImpl;
use macrify::ast_traits::module::IModule;
use macrify::ast_traits::strct::IStruct;
use macrify::macrify;

#[macrify(target = "struct")]
pub fn add_field_to_struct(target: &mut impl IStruct) {
    target.add_fields(syn::parse_quote! {
        pub field: usize,
    })
}

#[macrify(target = "mod")]
pub fn test_mod_stuff(target: &mut impl IModule) {
    let strct = target.add_struct(syn::parse_quote! {
        pub struct GeneratedStruct {}
    });

    strct.add_fields(syn::parse_quote! {
        pub field_one: usize,
        pub field_two: usize,
    });

    target.add_impl_block(syn::parse_quote! {
        #[macrify::macrify_marker(id = "0")]
        impl GeneratedStruct {
            pub fn get_field_one(&self) -> usize {
                self.field_one
            }
        }
    });

    target
        .get_impl_block_by_id_mut("0")
        .add_method(syn::parse_quote! {
            pub fn get_field_two(&self) -> usize {
                self.field_two
            }
        });
}
