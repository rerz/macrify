#[cfg(test)]
mod tests {
    use macrify_playground::*;

    #[test_mod_stuff]
    mod test_mod {}

    #[test]
    fn test() {
        let strct = test_mod::GeneratedStruct {
            field_one: 0,
            field_two: 1,
        };

        dbg!(strct.get_field_one());
        dbg!(strct.get_field_two());
    }
}
