#![feature(box_patterns)]
#![feature(let_chains)]

extern crate proc_macro;

use darling::FromMeta;
use proc_macro::TokenStream;
use quote::quote;
use syn::{
    parse_macro_input, punctuated::Punctuated, AttributeArgs, FnArg, ItemFn, ItemStruct, PatType,
    Visibility,
};

#[derive(darling::FromMeta)]
struct MacrifyArgs {
    target: String,
    #[darling(default)]
    args: Option<String>,
}

#[proc_macro_attribute]
pub fn macrify_marker(_: TokenStream, input: TokenStream) -> TokenStream {
    input
}

fn has_arg_with_ident(fun: &syn::ItemFn, ident: &str) -> bool {
    fun.sig.inputs.iter().any(|arg| {
        matches!(
        arg,
        FnArg::Typed(pat_type)
        if matches!(
            &pat_type.pat,
            box syn::Pat::Ident(pat_ident)
            if pat_ident.ident == syn::Ident::new("target", proc_macro2::Span::call_site()))
        )
    })
}

#[proc_macro_attribute]
pub fn macrify(args: TokenStream, input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as ItemFn);

    let input_ident = &input.sig.ident;

    // TODO: make args optional
    let macrify_args: MacrifyArgs =
        MacrifyArgs::from_list(&syn::parse_macro_input!(args as AttributeArgs))
            .expect("couldn't parse macrify args");

    // TODO: make this nicer
    let target_syn_ty = match macrify_args.target.as_ref() {
        "struct" => quote!(syn::ItemStruct),
        "mod" => quote!(syn::ItemMod),
        "impl" => quote!(syn::ItemImpl),
        _ => panic!("unsupported target {}", &macrify_args.target),
    };

    if !has_arg_with_ident(&input, "target") {
        panic!("no target arg");
    }

    if let Some(_) = &macrify_args.args {
        if !has_arg_with_ident(&input, "args") {
            panic!("no args arg");
        }
    }

    let mut original_fn = input.clone();

    // TODO: improve spans?
    original_fn.sig.ident = syn::Ident::new(
        &format!("__original_{}", input_ident),
        proc_macro2::Span::call_site(),
    );

    let original_ident = original_fn.sig.ident.clone();

    original_fn.vis = Visibility::Inherited;

    let call = match &macrify_args.args {
        Some(args) => {
            let args_ty = syn::Ident::new(&args, proc_macro2::Span::call_site());
            quote! {
                {
                    let args: #args_ty = #args_ty::from_list(&syn::parse_macro_input!(a as syn::AttributeArgs)).expect("failed to parse args");
                    #original_ident(&mut target, args);
                }
            }
        }
        None => quote! {
            {
                #original_ident(&mut target);
            }
        },
    };

    TokenStream::from(quote! {
        #original_fn

        #[proc_macro_attribute]
        pub fn #input_ident(a: proc_macro::TokenStream, i: proc_macro::TokenStream) -> proc_macro::TokenStream {
            use darling::FromMeta;
            use quote::ToTokens;
            use macrify::macrify_marker;

            let mut target = syn::parse_macro_input!(i as #target_syn_ty);

            #call

            target.into_token_stream().into()
        }
    })
}
