# macrify
## Yo dawg I heard you like macros...

This crate aims to make writing correct `proc_macro`s a little less tedious.

It achieves this by providing extensions traits for `syn` AST types which make accessing or modifying them much easier.

In addition, it provides the `macrify` macro which can be attached to normal `fn`s to make them act like decorators in `python` or `TypeScript` for example.

It is currently designed to generate `proc_macro_attribute`s but could probably be extended to generate `proc_macro_derive`s as well. 

## Example usage

Below you can find an example of how you would write macros using `macrify`. Additional examples can be found in the `macrify_playground` crate and examples of their usages can be found in the `macrify_playground/uses` crate.

```rust
#[macrify(target = "mod")]
pub fn add_struct_and_field(target: &mut impl IModule) {
    target
        .add_struct(syn::parse_quote! {
            pub struct Generated {}
        })
        .add_fields(syn::parse_quote! {
            pub some_field: usize
        });
}
```

The first thing to notice here is that macrify requires you to specify what type of program element your macro can be attached to.
This information is then used to take care of parsing the actual macro input `TokenStream` for you.
It also provides parsing of macro arguments out of the box powered by `darling`.
In this case the macro can be attached to modules and as such the function receives a mutable reference to some module.

The `IModule` trait here is implemented for `syn::ItemMod` and provides helper methods for accessing, modifying or adding `struct`s or `fn`s inside it.
These methods expect instances of the respective `syn` AST types, and the easiest way to get them would be through `syn::parse_quote!`.

The generated macro can then be used as such:

```rust
    #[add_struct_and_field]
    mod test_mod {}

    #[test]
    fn test() {
        let _ = test_mod::Generated { some_field: 0 };
    }
```

## ...so I put macros in your macros

The macrify macro basically expands to the following:

```rust
    #original_fn
    
    #[proc_macro_attribute]
    pub fn #input_ident(a: proc_macro::TokenStream, i: proc_macro::TokenStream) -> proc_macro::TokenStream {
        use darling::FromMeta;
        use quote::ToTokens;
    
        let mut target = syn::parse_macro_input!(i as #target_syn_ty);
    
        let args: #args_ty = #args_ty::from_list(&syn::parse_macro_input!(a as syn::AttributeArgs)).expect("failed to parse args");
        #original_ident(&mut target, args);
    
        target.into_token_stream().into()
    }
```

It replaces the annotated `fn` with a `proc_macro_attribute` implementation with the same name and keeps a private copy of the original `fn`.

Inside this `proc_macro_attribute` it parses the input and args (of the actual macro) to the specified types.

Afterwards it calls the original `fn` and then makes a `TokenStream` out of the modified `syn` AST type.
