use crate::MarkerArgs;
use darling::FromMeta;
use quote::ToTokens;
use syn::{Attribute, ItemFn, ItemImpl, ItemMod, Type};

// TODO: probably return results or options for most of these
pub trait IModule {
    fn raw(&self) -> &syn::ItemMod;

    fn raw_mut(&mut self) -> &mut syn::ItemMod;

    // fn stuff

    fn add_fn(&mut self, fun: syn::ItemFn) -> &mut syn::ItemFn;

    fn get_fn_by_ident(&self, ident: &str) -> &syn::ItemFn;

    fn get_fn_by_ident_mut(&mut self, ident: &str) -> &mut syn::ItemFn;

    // struct stuff

    fn add_struct(&mut self, strct: syn::ItemStruct) -> &mut syn::ItemStruct;

    fn get_struct_by_ident(&self, ident: &str) -> &syn::ItemStruct;

    fn get_struct_by_ident_mut(&mut self, ident: &str) -> &mut syn::ItemStruct;

    // module stuff

    fn add_module(&mut self, module: syn::ItemMod) -> &mut syn::ItemMod;

    fn get_mod_by_ident_mut(&mut self, ident: &str) -> &mut syn::ItemMod;

    // impl stuff

    fn add_impl_block(&mut self, block: syn::ItemImpl) -> &mut syn::ItemImpl;

    fn get_impl_blocks(&self) -> Vec<&syn::ItemImpl>;

    fn get_impl_blocks_mut(&mut self) -> Vec<&mut syn::ItemImpl>;

    fn get_impl_block_by_id(&self, id: &str) -> &syn::ItemImpl;

    fn get_impl_block_by_id_mut(&mut self, id: &str) -> &mut syn::ItemImpl;
}

// TODO: clean these up, lots of duplication

fn get_fn_with_ident<'a>(item: &'a syn::Item, ident: &str) -> Option<&'a syn::ItemFn> {
    match item {
        syn::Item::Fn(fun)
            if fun.sig.ident == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
        {
            Some(fun)
        }
        _ => None,
    }
}

fn get_fn_with_ident_mut<'a>(item: &'a mut syn::Item, ident: &str) -> Option<&'a mut syn::ItemFn> {
    match item {
        syn::Item::Fn(fun)
            if fun.sig.ident == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
        {
            Some(fun)
        }
        _ => None,
    }
}

fn get_struct_with_ident<'a>(item: &'a syn::Item, ident: &str) -> Option<&'a syn::ItemStruct> {
    match item {
        syn::Item::Struct(strct)
            if strct.ident == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
        {
            Some(strct)
        }
        _ => None,
    }
}

fn get_struct_with_ident_mut<'a>(
    item: &'a mut syn::Item,
    ident: &str,
) -> Option<&'a mut syn::ItemStruct> {
    match item {
        syn::Item::Struct(strct)
            if strct.ident == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
        {
            Some(strct)
        }
        _ => None,
    }
}

fn get_module_with_ident_mut<'a>(
    item: &'a mut syn::Item,
    ident: &str,
) -> Option<&'a mut syn::ItemMod> {
    match item {
        syn::Item::Mod(module)
            if module.ident == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
        {
            Some(module)
        }
        _ => None,
    }
}

impl IModule for syn::ItemMod {
    fn raw(&self) -> &ItemMod {
        self
    }

    fn raw_mut(&mut self) -> &mut ItemMod {
        self
    }

    fn add_fn(&mut self, fun: ItemFn) -> &mut ItemFn {
        let ident = fun.sig.ident.to_string();
        if let Some((brace, items)) = &mut self.content {
            items.push(syn::Item::Fn(fun));
        }

        self.get_fn_by_ident_mut(&ident)
    }

    fn get_fn_by_ident(&self, ident: &str) -> &ItemFn {
        if let Some((brace, items)) = &self.content {
            let fun = items
                .iter()
                .find_map(|item| get_fn_with_ident(item, ident))
                .expect(&format!("no such fn in this module: {}", ident));

            return fun;
        }

        panic!("no content")
    }

    fn get_fn_by_ident_mut(&mut self, ident: &str) -> &mut ItemFn {
        if let Some((brace, items)) = &mut self.content {
            let fun = items
                .iter_mut()
                .find_map(|item| get_fn_with_ident_mut(item, ident))
                .expect(&format!("no such fn in this module: {}", ident));

            return fun;
        }

        panic!("no content")
    }

    fn add_struct(&mut self, strct: syn::ItemStruct) -> &mut syn::ItemStruct {
        let ident = strct.ident.to_string();
        if let Some((brace, items)) = &mut self.content {
            items.push(syn::Item::Struct(strct));
        }

        self.get_struct_by_ident_mut(&ident)
    }

    fn get_struct_by_ident(&self, ident: &str) -> &syn::ItemStruct {
        if let Some((brace, items)) = &self.content {
            let strct = items
                .iter()
                .find_map(|item| get_struct_with_ident(item, ident))
                .expect(&format!("no such struct in this module: {}", ident));

            return strct;
        }

        panic!("no content")
    }

    fn get_struct_by_ident_mut(&mut self, ident: &str) -> &mut syn::ItemStruct {
        if let Some((brace, items)) = &mut self.content {
            let strct = items
                .iter_mut()
                .find_map(|item| get_struct_with_ident_mut(item, ident))
                .expect(&format!("no such struct in this module: {}", ident));

            return strct;
        }

        panic!("no content")
    }

    fn add_module(&mut self, module: ItemMod) -> &mut ItemMod {
        let ident = module.ident.to_string();
        if let Some((brace, items)) = &mut self.content {
            items.push(syn::Item::Mod(module));
        }

        self.get_mod_by_ident_mut(&ident)
    }

    fn get_mod_by_ident_mut(&mut self, ident: &str) -> &mut ItemMod {
        if let Some((brace, items)) = &mut self.content {
            let module = items
                .iter_mut()
                .find_map(|item| get_module_with_ident_mut(item, ident))
                .expect(&format!("no such struct in this module: {}", ident));

            return module;
        }

        panic!("no content")
    }

    fn add_impl_block(&mut self, block: ItemImpl) -> &mut ItemImpl {
        let id = get_impl_id(&block).to_owned();

        if let Some((brace, content)) = &mut self.content {
            content.push(syn::Item::Impl(block));
        }

        self.get_impl_block_by_id_mut(&id)
    }

    fn get_impl_blocks(&self) -> Vec<&ItemImpl> {
        if let Some((brace, content)) = &self.content {
            return content
                .iter()
                .filter_map(|item| match item {
                    syn::Item::Impl(item_impl) => Some(item_impl),
                    _ => None,
                })
                .collect();
        }

        panic!("no content")
    }

    fn get_impl_blocks_mut(&mut self) -> Vec<&mut ItemImpl> {
        if let Some((brace, content)) = &mut self.content {
            return content
                .iter_mut()
                .filter_map(|item| match item {
                    syn::Item::Impl(item_impl) => Some(item_impl),
                    _ => None,
                })
                .collect();
        }

        panic!("no content")
    }

    fn get_impl_block_by_id(&self, id: &str) -> &ItemImpl {
        self.get_impl_blocks()
            .iter()
            .find(|impl_block| impl_has_id(impl_block, id))
            .expect("no impl block with this id")
    }

    fn get_impl_block_by_id_mut(&mut self, id: &str) -> &mut ItemImpl {
        self.get_impl_blocks_mut()
            .into_iter()
            .find(|impl_block| impl_has_id(impl_block, id))
            .expect("no impl block with this id")
    }
}

fn impl_has_id(item: &ItemImpl, id: &str) -> bool {
    if let Some(attr) = get_attr_by_ident(&item.attrs, "macrify_marker") {
        let meta = attr.parse_meta().unwrap();
        let args: MarkerArgs = MarkerArgs::from_meta(&meta).unwrap();

        if args.id == id {
            return true;
        }
    }

    false
}

fn get_impl_id(item: &ItemImpl) -> String {
    if let Some(attr) = get_attr_by_ident(&item.attrs, "macrify_marker") {
        let meta = attr.parse_meta().unwrap();
        let args: MarkerArgs = MarkerArgs::from_meta(&meta).unwrap();

        return args.id;
    }

    panic!("no marker attribute")
}

fn get_attr_by_ident<'a>(attrs: &'a [Attribute], ident: &str) -> Option<&'a Attribute> {
    attrs
        .iter()
        .find(|attr| attr.path.segments.last().unwrap().ident.to_string() == ident)
}
