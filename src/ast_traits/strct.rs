use syn::punctuated::Punctuated;

// https://github.com/dtolnay/syn/issues/651#issuecomment-503771863
// Required because `syn::Field` does not implement `syn::Parse` by default
pub struct ParsableNamedField {
    pub field: syn::Field,
}

impl syn::parse::Parse for ParsableNamedField {
    fn parse(input: syn::parse::ParseStream<'_>) -> syn::parse::Result<Self> {
        let field = syn::Field::parse_named(input)?;

        Ok(ParsableNamedField { field })
    }
}

pub trait IStruct {
    fn add_fields(&mut self, punct_fields: Punctuated<ParsableNamedField, syn::Token![,]>);
}

impl IStruct for syn::ItemStruct {
    fn add_fields(&mut self, punct_fields: Punctuated<ParsableNamedField, syn::Token![,]>) {
        if let syn::Fields::Named(named) = &mut self.fields {
            for field in punct_fields {
                named.named.push(field.field);
            }
        }
    }
}
