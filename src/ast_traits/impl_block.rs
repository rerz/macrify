use syn::{Attribute, ImplItem, ImplItemMethod, ItemFn};

pub trait IImpl {
    fn add_method(&mut self, method: syn::ImplItemMethod) -> &mut syn::ImplItemMethod;

    fn get_method_by_ident_mut(&mut self, ident: &str) -> &mut syn::ImplItemMethod;
}

impl IImpl for syn::ItemImpl {
    fn add_method(&mut self, method: ImplItemMethod) -> &mut ImplItemMethod {
        let ident = method.sig.ident.to_string();

        self.items.push(ImplItem::Method(method));

        self.get_method_by_ident_mut(&ident)
    }

    fn get_method_by_ident_mut(&mut self, ident: &str) -> &mut ImplItemMethod {
        self.items
            .iter_mut()
            .find_map(|item| match item {
                syn::ImplItem::Method(method)
                    if method.sig.ident
                        == syn::Ident::new(ident, proc_macro2::Span::call_site()) =>
                {
                    Some(method)
                }
                _ => None,
            })
            .expect("no such method")
    }
}
