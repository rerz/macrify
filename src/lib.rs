#![feature(box_patterns)]

pub mod ast_traits;

pub use macrify_macros::*;

#[derive(darling::FromMeta)]
pub struct MarkerArgs {
    pub id: String,
}
